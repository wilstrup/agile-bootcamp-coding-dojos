package controllers;

import models.TodoList;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.todoIndex;

import static play.libs.Json.toJson;

public class TodoController extends Controller {
    private static TodoList todoList = new TodoList();

    public static Result index() {
        return ok(todoIndex.render());
    }

    public static Result todos() {
        return ok(toJson(todoList.all()));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result newTodo() {
        models.Todo t = Json.fromJson(request().body().asJson(), models.Todo.class);
        if(t != null) {
            todoList.create(t);
            return ok();
        }
        return badRequest();
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result updateTodo(Integer id) {
        models.Todo t = Json.fromJson(request().body().asJson(), models.Todo.class);
        if(t != null) {
            todoList.get(id).setCompleted(t.isCompleted());
            return ok();
        }
        return badRequest();
    }

    public static Result deleteTodo(Integer id) {
        todoList.delete(id);
        return ok();
    }
}
