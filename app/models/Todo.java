package models;

public class Todo {
    private Integer id;
    private String label;
    private boolean completed;

    public Todo(String label) {
        this(label, false);
    }

    public Todo(String label, boolean completed) {
        this.completed = completed;
        this.label = label;
    }

    public Todo() {}

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}