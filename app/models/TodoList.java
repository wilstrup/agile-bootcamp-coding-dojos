package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TodoList {
    private Map<Integer, Todo> todoList = new HashMap<>();
    private int currentId = 1;

    public TodoList() {
        Todo todo = new Todo("Example Todo");
        this.create(todo);
    }

    public List<Todo> all() {
        return new ArrayList<>(todoList.values());
    }

    public Todo get(Integer id) {
        return todoList.get(id);
    }

    public void create(Todo todo) {
        int id = currentId++;
        todo.setId(id);
        todoList.put(id, todo);
    }

    public void delete(Integer id) {
        if(todoList.containsKey(id)) todoList.remove(id);
    }
}
