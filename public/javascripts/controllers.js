(function() {
    "use strict";
    var server = new Server();

    ko.applyBindings(new TodoViewModel(server));
})();
