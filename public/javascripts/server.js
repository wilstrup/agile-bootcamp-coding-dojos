var Server = function() {
    "use strict";
    var self = this;

    self.getTodos = function getTodos(callback) {
        $.getJSON("/todo/todos", callback);
    };

    self.createTodo = function createTodos(todo, callback) {
        $.ajax({
            url: "/todo/todos",
            contentType: "application/json; charset=utf-8",
            data: ko.mapping.toJSON(todo),
            dataType: "json",
            type: "POST",
            complete: callback
        });
    };

    self.updateTodo = function updateTodo(todo, callback) {
        $.ajax({
            url: "/todo/todos/" + todo.id(),
            contentType: "application/json; charset=utf-8",
            data: ko.mapping.toJSON(todo),
            dataType: "json",
            type: "POST",
            complete: callback
        });
    };

    self.removeTodo = function removeTodo(todo, callback) {
        $.ajax({
            url: "/todo/todos/" + todo.id() + "/delete",
            type: "POST",
            complete: callback
        });
    };
};
