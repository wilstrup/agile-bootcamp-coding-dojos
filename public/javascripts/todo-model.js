var TodoModel = function TodoModel(data, server) {
    "use strict";
    var self = this;
    self.server = server;

    ko.mapping.fromJS(data, {}, this);

    self.completed.subscribe(function() {
        self.server.updateTodo(self)
    }, self, 'change');
};

var TodoModelMapping = function(server) {
    return {
        ignore: ["server"],
        create: function(options) {
            return new TodoModel(options.data, server);
        },
        key: function(item) {
            return ko.utils.unwrapObservable(item.id);
        }
    };
};
