(function () {
    "use strict";
    module("Todo View Model Test", {
        setup: function setup() {
            var self = this;

            self.createdTodo = "";
            self.removedTodo = "";
            self.server = {
                getTodos: function(callback) {
                    callback([
                        {
                            label: "Awesome",
                            id: 1,
                            completed: false
                        },
                        {
                            label: "More Awesome",
                            id: 2,
                            completed: true
                        }
                    ]);
                },
                createTodo: function(todo, callback) {
                    self.createdTodo = todo.label;
                },
                removeTodo: function(todo, callback) {
                    self.removedTodo = todo.label()
                }
            };

            self.viewModel = new TodoViewModel(self.server);
        }
    });

    test("Todo List", function() {
        strictEqual(this.viewModel.todos().length, 2, "Make sure the viewModel automatically got a list of todos from the server");
    });

    test("Add Todo", function() {
        strictEqual(this.viewModel.todos().length, 2, "Check that the number of todos is as expected");
        strictEqual(this.createdTodo, "", "Check that no todos have been created already");

        this.viewModel.newTodo("New Todo");
        this.viewModel.createTodo();

        strictEqual(this.viewModel.todos().length, 3, "A new todo has been added");
        strictEqual(this.viewModel.newTodo(), "", "And that the newTodo field has been cleared");
        strictEqual(this.viewModel.todos()[2].label(), "New Todo", "And check that it got the right title");

        strictEqual(this.createdTodo, "New Todo", "Finally check that the server has been notified of the new todo")
    });

    test("Remove Todo", function() {
        strictEqual(this.viewModel.todos().length, 2, "Make sure the todos have not been tampered with");
        strictEqual(this.removedTodo, "", "Also make sure no todos have been removed");

        this.viewModel.removeTodo(this.viewModel.todos()[1]);

        strictEqual(this.viewModel.todos().length, 1, "Now a todo should have disappeared");
        strictEqual(this.viewModel.todos()[0].label(), "Awesome", "Make sure that the right was removed");

        strictEqual(this.removedTodo, "More Awesome", "Finally make sure that the server was notified properly")
    });
})();