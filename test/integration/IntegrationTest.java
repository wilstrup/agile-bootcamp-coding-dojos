package integration;

import org.junit.Test;
import play.libs.WS;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

public class IntegrationTest {
    @Test
    public void testIndex() {
        running(testServer(3333), new Runnable() {
            @Override
            public void run() {
                WS.Response response = WS.url("http://localhost:3333/").get().get();
                assertEquals(200, response.getStatus());
                assertTrue(response.getBody().contains("Hello World!"));
            }
        });
    }

    @Test
    public void testTodoIndex() {
        running(testServer(3333), new Runnable() {
            @Override
            public void run() {
                WS.Response response = WS.url("http://localhost:3333/todo").get().get();
                assertEquals(200, response.getStatus());
                assertTrue(response.getBody().contains("Todo List"));
            }
        });
    }

    @Test
    public void testTodos() {
        running(testServer(3333), new Runnable() {
            @Override
            public void run() {
                WS.Response response = WS.url("http://localhost:3333/todo/todos").get().get();
                assertEquals(200, response.getStatus());
                assertTrue(response.getBody().contains("Example Todo"));
            }
        });
    }
}
