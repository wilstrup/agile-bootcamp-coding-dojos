package models;

import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TodoTest {
    @Test
    public void todoCreation() {
        Todo todo = new Todo("Label");

        assertEquals("Label", todo.getLabel());
        assertNull(todo.getId());
        assertFalse(todo.isCompleted());
    }

    @Test
    public void todoSpecificCreation() {
        Todo todo = new Todo("Label", true);

        assertEquals("Label", todo.getLabel());
        assertNull(todo.getId());
        assertTrue(todo.isCompleted());
    }

    @Test
    public void todoSetId() {
        Todo todo = new Todo("Label");
        assertNull(todo.getId());

        todo.setId(1);
        assertEquals((Integer)1, todo.getId());
    }

    @Test
    public void todoSetCompleted() {
        Todo todo = new Todo("Label");
        assertFalse(todo.isCompleted());

        todo.setCompleted(true);
        assertTrue(todo.isCompleted());
    }
}
